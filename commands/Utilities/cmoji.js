const { getEmoji, getAllEmoji, getThemes } = require('random-text-meme');
const Discord = require("discord.js");
const fs = require("fs");
const dateFormat = require("dateformat");
exports.run = async (bot, message, args, functions) => {

  let argscmd = message.content.split(" ").slice(1);
  let word = argscmd.slice(0).join(" "); // yes, start at 0, not 1. I hate that too.
  //if (!word) return message.reply("Please give me a word to look up.");
  message.delete();
  const date = require('date-and-time');
  const now = new Date();
  const channel = bot.channels.cache.find(channel => channel.name === "bot-logs")
  channel.send({
    "embed": {
      "color": 2899536,
      "footer": {
        "text": date.format(now, 'MM/DD/YYYY hh:mm:ss')
      },
      "fields": [

        {
          "name": "[RavenBot LOG]",
          "value": message.author.username + ` ran \cMoji - Param: ` + word,
          "inline": true
        }
      ]
    }
  });


  message.channel.send(getEmoji());


}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['c'],
  permLevel: "User"
};

exports.help = {
  name: "cmoji",
  category: "Troll",
  description: "Returns a Cute Emoji",
  usage: "cmoji"
};
