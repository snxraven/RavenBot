var youtubedl = require('youtube-dl-exec');
process.env["NO_PROXY"] = "*";
var getYoutubeTitle = require('get-youtube-title')
const getVideoId = require('get-video-id');
var youtubeThumbnail = require('youtube-thumbnail');
const pify = require('pify');
exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars

  let argscmd = message.content.split(" ").slice(1);
  let cmd = argscmd[0]; // yes, start at 0, not 1. I hate that too.
  if (!cmd) return message.reply("Please provide me a Youtube URL!");
  let vidID = getVideoId(cmd);
  var thumbnail = youtubeThumbnail(cmd);
  message.reply("Starting download, please wait...")

  getYoutubeTitle(vidID.id, function (err, title) {
    if (!title) return message.channel.send("We are sorry, but it seems the API is not working for us right now, try again soon.")
    // console.log(title) // 'SLCHLD - EMOTIONS (feat. RIPELY) (prod. by GILLA)'
    var name = Math.floor(Math.random() * 99999999999999);

    // console.log(title) // 'SLCHLD - EMOTIONS (feat. RIPELY) (prod. by GILLA)'
    const fileTitle = toSeoUrl(title) + "-" + name;
    console.log(fileTitle);
    //message.delete(0);
    const jsonfile = require('jsonfile')

    const cmd = require('cmd-promise')


    cmd("cd ./node_modules/youtube-dl/bin/ && ./youtube-dl -x --audio-format mp3 -o /var/www/html/ytdl/audio/" + fileTitle + ".mp3 " + argscmd).then(out => {
      // Check the length
      var cmdLength = out.stdout.length;

      message.channel.send({
        "embed": {
          "color": 3066993,
          "footer": {
            "icon_url": "https://cdn.discordapp.com/avatars/342128351638585344/e5c483abb70234a4e721fb329a8e1fcc.png?size=128",
            "text": "made By: SNXRaven#8205"
          },
          "thumbnail": {
            "url": thumbnail.default.url
          },

          "fields": [

            {
              "name": "Youtube MP3 Downloader",
              "value": "[Download](https://yt-audio.codingvm.codes/" + fileTitle + ".mp3)",
              "inline": true
            }
          ]
        }
      });
    }).catch(err => {
      //  console.log('err =', err.toString())
      message.channel.send('\`\`\`js\n' + err.toString() + '\`\`\`')
    })

  });
};

function toSeoUrl(url) {
  return url.toString() // Convert to string
    .normalize('NFD') // Change diacritics
    .replace(/[\u0300-\u036f]/g, '') // Remove illegal characters
    .replace(/\s+/g, '-') // Change whitespace to dashes
    .toLowerCase() // Change to lowercase
    .replace(/&/g, '-and-') // Replace ampersand
    .replace(/[^a-z0-9\-]/g, '') // Remove anything that is not a letter, number or dash
    .replace(/-+/g, '-') // Remove duplicate dashes
    .replace(/^-*/, '') // Remove starting dashes
    .replace(/-*$/, ''); // Remove trailing dashes
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "ytdl-mp3",
  category: "Downloaders",
  description: "Download youtube videos as MP3",
  usage: "ytdl-mp3 [...code]"
};