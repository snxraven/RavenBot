var unirest = require('unirest');
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
let reqNumber;
let imgURL;

const isNotDefined = require("is-not-defined");

const Discord = require("discord.js");
const fs = require("fs");
const dateFormat = require("dateformat");
exports.run = async (bot, message, args, functions) => {

    let argscmd = message.content.split(" ").slice(1);
    let word = argscmd.slice(0).join(" ");
    if (!word) return message.reply("Please give me a search word.");
    message.delete();

    const date = require('date-and-time');
    const now = new Date();
   const channel = bot.channels.cache.find(channel => channel.name === process.env.LOGSCHANNELNAME)
     channel.send({
      "embed": {
        "color": 2899536,
        "footer": {
          "text": date.format(now, 'MM/DD/YYYY hh:mm:ss')
        },
        "fields": [

          {
            "name": "[CHKSRV LOG]",
            "value": message.author.username + ` ran \nUrban - Param: ` + word,
            "inline": true
          }
        ]
      }
    });


    unirest.post("https://pixabay.com/api/?key=" + process.env.PIXABAYAPIKEY + "&q=" + word + "&image_type=photo")
        .header("Accept", "application/json") 
        .end(function(result) {
            // For debug Only
            //  console.log(result.status, result.headers, result.body);
            console.log("Running Command for Google Images | Params: " + word);

            reqNumber = getRandomInt(Object.keys(result.body.hits).length)

            //console.log(result.body.hits[reqNumber])

            if (isNotDefined(result.body.hits)) {
            return message.reply("Sorry, I could not find anything for that keyword")
            }


              message.channel.send(result.body.hits[reqNumber].largeImageURL);
        });
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "i",
  category: "Troll",
  description: "Image Search",
  usage: "i [search words]"
};
