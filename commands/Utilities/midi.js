exports.run = async (bot, message, args, functions) => {
    let argscmd = message.content.split(" ").slice(1);
    let urlcmd = argscmd[0];

    if (!urlcmd) return message.reply("Please give me a MIDI URL or command");


    var fs = require('fs');
    const synth = require('synth-js');
    var nrc = require('node-run-cmd');

    var download = require('download-file')

    var url = urlcmd
    if (url != "stop") {
        var name = Math.floor(Math.random() * 999999);
        var callback = function (exitCodes) {
            console.log('Called Back - Converting....');

            let midiBuffer = fs.readFileSync('./midifiles/' + name + ".mid");
            // convert midi buffer to wav buffer
            let wavBuffer = synth.midiToWav(midiBuffer).toBuffer();

            fs.writeFileSync('/var/www/html/midi/' + name + ".wav", wavBuffer, {
                encoding: 'binary'
            });
            if (message.member.voice.channel) {

                message.member.voice.channel.join()
                    .then(connection => { // Connection is an instance of VoiceConnection
                        console.log('Sending and Playing');
                        const date = require('date-and-time');
                        const now = new Date();
                        message.channel.send({
                            "embed": {
                                "color": 2899536,
                                "footer": {
                                    "text": date.format(now, 'MM/DD/YYYY hh:mm:ss')
                                },
                                "fields": [

                                    {
                                        "name": "Midi Sound Generator",
                                        "value": "Here is the sound file: \n https://midi.codingvm.codes/" + name + ".wav \n It should also be playing within the voice channel",
                                        "inline": true
                                    }
                                ]
                            }
                        });

                        const dispatcher = connection.play('/var/www/html/midi/' + name + ".wav");
                        dispatcher.on("end", end => {
                            message.member.voice.channel.leave();
                        });

                        dispatcher.on('error', e => {
                            // Catch any errors that may arise
                            return message.reply(e);

                        });

                    })

                    .catch(console.log);


            } else {

                const date = require('date-and-time');
                const now = new Date();
                message.channel.send({
                    "embed": {
                        "color": 2899536,
                        "footer": {
                            "text": date.format(now, 'MM/DD/YYYY hh:mm:ss')
                        },
                        "fields": [

                            {
                                "name": "Midi Sound Generator",
                                "value": "Here is the sound file: \n https://midi.codingvm.codes/" + name + ".wav \n If you join voice, I can also play it when finished.",
                                "inline": true
                            }
                        ]
                    }
                });
            }

        }

        message.reply('Please wait, Converting your file...');
        nrc.run('wget ' + url + ' -O ./midifiles/' + name + '.mid', {
            onDone: callback
        });

        console.log('wget ' + url + ' -O ./midifiles/' + name + '.mid');
    }

    if (url == "stop") {
        message.reply('I have stopped...');
        console.log('Stopping...');
        message.member.voice.channel.leave();


    }


}


exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [''],
    permLevel: "User"
};

exports.help = {
    name: "midi",
    category: "Troll",
    description: "Play a midi file from a URL - Download the wav or Listen in VC",
    usage: "midi"
};
