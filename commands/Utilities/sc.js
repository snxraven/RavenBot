const { getEmoji, getAllEmoji, getThemes } = require('random-text-meme');
const Discord = require("discord.js");
const fs = require("fs");
const dateFormat = require("dateformat");
require('dotenv').config()
var name = Math.floor(Math.random() * 99999999999999);

exports.run = async (bot, message, args, functions) => {

  let argscmd = message.content.split(" ").slice(1);
  let word = argscmd.slice(0).join(" "); // yes, start at 0, not 1. I hate that too.
  //if (!word) return message.reply("Please give me a word to look up.");
  //message.delete();
  const scdl = require('soundcloud-downloader').default
  const fs = require('fs')
  
  const SOUNDCLOUD_URL = word
  const CLIENT_ID = process.env.ID
  
  scdl.download(SOUNDCLOUD_URL).then(stream => stream.pipe(fs.createWriteStream('/var/www/html/ytdl/scdl/'+name+'.mp3')))
  
  
  message.channel.send({
    "embed": {
      "color": 3066993,
      "footer": {
        "icon_url": "https://cdn.discordapp.com/avatars/342128351638585344/e5c483abb70234a4e721fb329a8e1fcc.png?size=128",
        "text": "made By: SNXRaven#8205"
      },
      "fields": [

        {
          "name": "SoundCloud MP3 Downloader",
          "value": "[Download](https://sc-dl.codingvm.codes/" + name + ".mp3)",
          "inline": true
        }
      ]
    }
  });
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['c'],
  permLevel: "User"
};

exports.help = {
  name: "scdl",
  category: "Downloaders",
  description: "Download a track from SoundCloud",
  usage: "scdl"
};
