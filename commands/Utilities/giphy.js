var unirest = require('unirest');
var giphy = require('apigiphy');
const { Client, MessageAttachment } = require('discord.js');

giphy = giphy({
  api_key: process.env.GIFYAPIKEY
});
const Discord = require("discord.js");
const fs = require("fs");
const dateFormat = require("dateformat");
exports.run = async (bot, message, args, functions) => {

  let argscmd = message.content.split(" ").slice(1);
  let word = argscmd.slice(0).join(" ");
  if (!word) return message.reply("Please give me a search word.");
  message.delete();

  const date = require('date-and-time');
  const now = new Date();

  const channel = bot.channels.cache.find(channel => channel.name === "bot-logs")
  channel.send({
    "embed": {
      "color": 2899536,
      "footer": {
        "text": date.format(now, 'MM/DD/YYYY hh:mm:ss')
      },
      "fields": [

        {
          "name": "[RavenBot LOG]",
          "value": message.author.username + ` ran \nGiphy Gif Search - Param: ` + word,
          "inline": true
        }
      ]
    }
  });

  // Random gif by tag using promise
  giphy.random({ tag: word })
    .then(function (response) {

      if (!response.data.image_original_url) return message.channel.send("Sorry, there was nothing found!");
      console.log(response.data.image_original_url);
      // http://s3.amazonaws.com/giphymedia/media/2jlDkaipGfgGc/200_d.gif
      // message.channel.send(response.data.image_original_url);
      const attachment = new MessageAttachment(response.data.image_original_url);
      // Send the attachment in the message channel
      message.channel.send(attachment);
    }, function (error) {
      console.log(error);
    });
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "gif",
  category: "Troll",
  description: "Gif Search from giphy",
  usage: "gif"
};
